<?php 
class VerazModel extends Mysql 
{
	public $strIdentificacion;
	public $strNombre;
	public $strSexo;

	public function __construct()
	{
		parent::__construct();
	}	

	public function selectVeraz()
	{
		$whereAdmin = "";
		if($_SESSION['idUser'] != 1 ){
			$whereAdmin = "";
		}
	 	$sql = "SELECT id_veraz,documento,nombre,score,fecha,status FROM veraz;";

		$request = $this->select_all($sql);
		return $request;
	}

	public function insertVeraz($identificacion, $nombre, $scoreParse, $sexo){
        $this->strIdentificacion = $identificacion;
        $this->strNombre = $nombre;
		$this->intScore = $scoreParse;
        $this->strSexo = $sexo;
     

        $query_insert  = "INSERT INTO veraz(documento,nombre,score,sexo,status) 
                            VALUES(?,?,?,?,1)";
        $arrData = array($this->strIdentificacion,
						$this->strNombre,
                        $this->intScore,
                        $this->strSexo);

        $request_insert = $this->insert($query_insert,$arrData);
        $return = $request_insert;
 
        return $return;
	}

	public function selectVerVeraz(int $id_veraz){
		$this->intIdVeraz = $id_veraz;
		$sql = "SELECT id_veraz, documento, nombre, score, sexo, fecha status FROM veraz WHERE id_veraz = $this->intIdVeraz";
		$request = $this->select($sql);
		return $request;
	}
}


 ?>
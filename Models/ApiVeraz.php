<?php


class ApiVeraz {
    public function __construct()
    {
    }

    public function getScore($identificacion, $nombre, $sexo)
	{
		$archivo = 'Libraries/Core/xml/consulta_veraz_'.date("dmYHis").'.xml';
        $this->strIdentificacion = $identificacion;
        $this->strNombre = $nombre;
        $this->strSexo = $sexo;
        $headers = array (    
            'HTTP/1.1 200 OK','Content-Type: text/html;charset=UTF-8','Server: cloudflare-nginx','Transfer-Encoding: chunked','Connection: keep-alive'
        );      
        include("Libraries/Core/xml/xml_envio.php");
        $ch = curl_init(URL_VERAZ);
        if (!$ch) { die("No se pudo inicializar la curl");}        
        curl_setopt($ch, CURLOPT_URL, URL_VERAZ);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "par_xml=". $par_xml);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch); // Ejecutar
        //Guardadno archivo
        file_put_contents($archivo, $result);      //mostrar respuesta
        curl_close($ch);
        // Buscar archivo generado para  formatear
        $mensaje = new SimpleXMLElement(file_get_contents($archivo));
        $Error = $mensaje->estado->codigoError;                
        if($Error != 0){
            $respuesta = false;
        } else {
            $respuesta = (int)$mensaje->respuesta->integrante->variables->variable[14]->valor;            
        }

        return $respuesta;
	}
}
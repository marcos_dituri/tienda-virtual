<?php
    if($response < 200){
    $htmlEstado = '<span class="badge badge-primary">
                    <i class="fas fa-ban"></i>
                    <strong> DESAPROBADO!</strong></span>';
    $htmlScore = '<span class="badge badge-primary">'.$response.'</span>';
    } else {
        $htmlEstado = '<span class="badge badge-success">
                        <i class="fas fa-check-circle"></i>
                        <strong>APROBADO</strong></span>';
        $htmlScore = '<span class="badge badge-success">'.$response.'</span>';
    }

    $respuesta = '<div class="card">
        <div class="body project_report">
            <div class="table-responsive">
                <table class="table m-b-0 table-hover">
                    <thead>
                        <tr>
                            <th>Score</th>
                            <th>Datos</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                '.$htmlScore.'
                            </td>
                            <td class="project-title">
                                <h6><a href="#">'.$strIdentificacion.'</a></h6>
                                <small>'.$strNombre.'</small>
                            </td>
                            <td>
                                '.$htmlEstado.'           
                            </td>
                        </tr>                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>';
?>
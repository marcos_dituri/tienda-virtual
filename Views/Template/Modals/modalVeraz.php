<!-- Modal -->
<div class="modal fade" id="modalFormVeraz" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Nuva consulta</h4>
            </div>
            <form action="" id="formVeraz" name="formVeraz" class="form-horizontal">
                <div class="modal-body">
                    <div class="body">
                        <div class="demo-masked-input">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <b>DNI/CUIT</b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="far fa-id-card"></i> </span>
                                        <input name="txtIdentificacion" id="txtIdentificacion" type="text" class="form-control date" placeholder="Ej: 39374547">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12"> <b>Nombre y apellido</b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="far fa-user"></i></span>                                        
                                        <input name="txtNombre" id="txtNombre" type="text" class="form-control time24 form-control-danger" placeholder="Ej: David Jonathan">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12"> <b>Sexo</b>
                                    <div class="input-group">
                                        <div class="radio inlineblock m-r-20">
                                            <input type="radio" name="txtSexo" id="Masculino" class="with-gap" value="M" checked="">
                                            <label for="Masculino">Masucilo</label>
                                        </div>                                
                                        <div class="radio inlineblock">
                                            <input type="radio" name="txtSexo" id="Femenino" class="with-gap" value="F" >
                                            <label for="Femenino">Femenino</label>
                                        </div>
                                        <div class="radio inlineblock">
                                            <input type="radio" name="txtSexo" id="Sociedad" class="with-gap" value="S">
                                            <label for="Sociedad">Sociedad</label>
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-default btn-round waves-effect">Enviar consulta</button>
                    <button type="button" class="btn btn-danger btn-simple btn-round waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalViewVeraz" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header header-primary">
        <h5 class="modal-title" id="titleModal">Datos del usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
          <tbody>
            <tr>
              <td>Identificación:</td>
              <td id="verDocumento">654654654</td>
            </tr>
            <tr>
              <td>Nombres:</td>
              <td id="verNombre">Jacob</td>
            </tr>
            <tr>
              <td>Score:</td>
              <td id="verScore">Jacob</td>
            </tr>
            <tr>
              <td>Sexo:</td>
              <td id="verSexo">Larry</td>
            </tr>
            <tr>
              <td>Fecha registro:</td>
              <td id="verFecha">Larry</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<?php 
	require_once("Models/ApiVeraz.php");
	class Veraz extends Controllers{
		public function __construct()
		{
			parent::__construct();
			session_start();			
			if(empty($_SESSION['login']))
			{
				header('Location: '.base_url().'/login');
			}
			getPermisos(3);
			$this->apiVeraz = new ApiVeraz();
		}

		public function Veraz()
		{
			if(empty($_SESSION['permisosMod']['r'])){
				header("Location:".base_url().'/dashboard');
			}			
			$data['page_tag'] = "Consultar Veraz - Tienda Virtual";
			$data['page_title'] = "Consultar Veraz - Tienda Virtual";
			$data['page_name'] = "Veraz";
			$data['page_functions_js'] = "functions_veraz.js";
			$this->views->getView($this,"veraz",$data);
		}

        public function getVerazApi() // Busca score en api y guarda en Sql
		{
			if($_SESSION['permisosMod']['w']){
				if($_POST){		
					if(empty($_POST['txtIdentificacion']) || empty($_POST['txtNombre']) || empty($_POST['txtSexo'])){
						$arrResponse = array("status" => false, "msg" => 'Datos incorrectos.');
					}
					else {
						$strIdentificacion = strClean($_POST['txtIdentificacion']);
						$strNombre = strClean($_POST['txtNombre']);
						$strSexo = strClean($_POST['txtSexo']);		
		
						if($strIdentificacion > 7)
						{
							$response = $this->apiVeraz->getScore($strIdentificacion, $strNombre, $strSexo);

							if($response !== false){								
								$request_user = $this->model->insertVeraz($strIdentificacion,
								$strNombre, 
								$response, 		
								$strSexo);					

								include("Views/Veraz/datosVeraz.php");
							}

							if($response == false)
							{
								$arrResponse = array('status' => false, 'msg' => 'Datos mal cargados.');
							}else{
								$arrResponse = array('status' => true, 'msg' => [$respuesta]);
							}
							echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
						}
					}										
				} 
			}	
			die();
		}

		public function getVeraz()
		{
			if($_SESSION['permisosMod']['r']){
				$arrData = $this->model->selectVeraz();
				
				for ($i=0; $i < count($arrData); $i++) {
					$btnView = '';
					$btnEdit = '';
					$btnDelete = '';

					if($_SESSION['permisosMod']['r']){
						$btnView = '<button class="btn btn-info btn-sm btnViewUsuario" onClick="fntViewVeraz('.$arrData[$i]['id_veraz'].')" title="Ver usuario"><i class="far fa-eye"></i></button>';
					}
					if($_SESSION['permisosMod']['d']){						
						$btnDelete = '<button class="btn btn-danger btn-sm btnDelUsuario" onClick="fntDelUsuario('.$arrData[$i]['id_veraz'].')" title="Eliminar usuario"><i class="far fa-trash-alt"></i></button>';						
					}
					$arrData[$i]['options'] = '<div class="text-center">'.$btnView.' '.$btnEdit.' '.$btnDelete.'</div>';
				}
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			
				die();
			}			
		
		}

		public function verVeraz($id_veraz){
			if($_SESSION['permisosMod']['r']){
				$id_veraz = intval($id_veraz);
				if($id_veraz > 0)
				{
					$arrData = $this->model->selectVerVeraz($id_veraz);
					if(empty($arrData))
					{
						$arrResponse = array('status' => false, 'msg' => 'Datos no encontrados.');
					}else{
						$arrResponse = array('status' => true, 'data' => $arrData);
					}
					echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
				}
			}
			die();
		}
	}
 ?>
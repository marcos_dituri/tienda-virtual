<?php
$par_xml ='<?xml version="1.0" encoding="ISO-8859-1"?>
<mensaje>
    <identificador>
        <userlogon>
            <matriz><![CDATA['.MATRIZ_VERAZ.']]></matriz>
            <usuario><![CDATA['.USUARIO_VERAZ.']]></usuario>
            <password><![CDATA['.PASSWORD_VERAZ.']]> </password>
        </userlogon>
        <medio></medio>
        <formatoInforme>'.FORMATO_INFORME_VERAZ.'</formatoInforme>
        <reenvio/>
        <producto>Experto</producto>
        <lote>
            <sectorVeraz>'.SECTOR_VERAZ.'</sectorVeraz>
            <sucursalVeraz>'.SUCURSAL_VERAZ.'</sucursalVeraz>
            <cliente>'.CLIENTE_VERAZ.'</cliente>
            <fechaHora>'.FECHA_HORA_VERAZ.'</fechaHora>
        </lote>
    </identificador>
    <consulta>
        <integrantes>1</integrantes>
        <integrante valor="1">
            <nombre><![CDATA['.$nombre.']]> </nombre>
            <sexo>'.$sexo.'</sexo>
            <documento><![CDATA['.$identificacion.']]> </documento>
        </integrante>
    </consulta>
</mensaje>';

?>
<?php


class VerazConsulta extends ApiVeraz
{
    function __construct()
    {
        $this->ApiVeraz = new ApiVeraz();
    }
    public function obtenerScore(string $identificacion, string $nombre, string $sexo)
    {
        $this->strIdentificacion = $identificacion;
        $this->strNombre = $nombre;
        $this->strSexo = $sexo;

        $consultaVeraz = $this->ConsultarApi($identificacion, $nombre, $sexo);

        return $consultaVeraz;
    }    

    public function setVeraz(string $identificacion,int $score, string $nombre, string $sexo, string $fecha){
        $this->strIdentificacion = $identificacion;
        $this->strNombre = $nombre;
        $this->strScore = $score;
        $this->strSexo = $sexo;
        $this->strFecha = $fecha;    

        $consultaVeraz = $this->ConsultarApi($identificacion, $nombre, $sexo);

        return $consultaVeraz;
    }

    public function consultaVeraz(){
        echo $consultaVeraz = $this->selectVeraz();

        exit;
        return $consultaVeraz;
    }
};

?>


let divLoading = document.querySelector("#divLoading");

document.addEventListener('DOMContentLoaded', function(){
    tableVeraz = $('#tableVeraz').dataTable( {
        "aProcessing":true,
        "aServerSide":true,        
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
        },
        "ajax":{
            "url": " "+base_url+"/Veraz/getVeraz",
            "dataSrc":""
        },
        "columns":[
            {"data":"id_veraz"},
            {"data":"documento"},
            {"data":"nombre"},
            {"data":"score"},
            {"data":"fecha"},
            {"data":"options"}
        ],
        'dom': 'lBfrtip',
        'buttons': [
            {
                "extend": "copyHtml5",
                "text": "<i class='far fa-copy'></i> Copiar",
                "titleAttr":"Copiar",
                "className": "btn btn-secondary"
            },{
                "extend": "excelHtml5",
                "text": "<i class='fas fa-file-excel'></i> Excel",
                "titleAttr":"Esportar a Excel",
                "className": "btn btn-success"
            },{
                "extend": "pdfHtml5",
                "text": "<i class='fas fa-file-pdf'></i> PDF",
                "titleAttr":"Esportar a PDF",
                "className": "btn btn-danger"
            },{
                "extend": "csvHtml5",
                "text": "<i class='fas fa-file-csv'></i> CSV",
                "titleAttr":"Esportar a CSV",
                "className": "btn btn-info"
            }
        ],
        "resonsieve":"true",
        "bDestroy": true,
        "iDisplayLength": 10,
        "order":[[0,"desc"]]  
    });

    
    if(document.querySelector("#formVeraz")){
        let formVeraz = document.querySelector("#formVeraz");
        formVeraz.onsubmit = function(e) {
            e.preventDefault();
            let strIdentificacion = document.querySelector('#txtIdentificacion').value;
            let strNombre = document.querySelector('#txtNombre').value;
            let strSexo =document.querySelector('input[name="txtSexo"]:checked').value;
                      
            if(strIdentificacion == '' || strNombre == '' || strSexo == '')
            {
                //swal("Atención", "Todos los campos son obligatorios." , "error");
                Swal.fire({
                    title: 'Atención!',
                    text: 'Todos los campos son obligatorios.',
                    icon: 'error',
                    confirmButtonText: 'Ok'  
                  })
                return false;
            } 

            let elementsValid = document.getElementsByClassName("valid");
            for (let i = 0; i < elementsValid.length; i++) { 
                if(elementsValid[i].classList.contains('is-invalid')) { 
                    swal("Atención", "Por favor verifique los campos en rojo." , "error");
                    return false;
                } 
            } 
            divLoading.style.display = "flex";
            let request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
            let ajaxUrl = base_url+'/Veraz/getVerazApi'; 
            let formData = new FormData(formVeraz);
            request.open("POST",ajaxUrl,true);
            request.send(formData);
            request.onreadystatechange = function(){
                if(request.readyState == 4 && request.status == 200){
                    let objData = JSON.parse(request.responseText);
                    if(objData.status)
                    {
                        $('#modalformVeraz').modal("hide");
                        formVeraz.reset();                    
                        swal.fire("Veraz encontrados", objData.msg[0],"success");                          
            
                    }else{
                        swal.fire("Hubo un Error", objData.msg , "error");
                    }
                }
                divLoading.style.display = "none";
                return false;
            }
        }
    }

}, false);


function fntViewVeraz(id_veraz){
    let request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    let ajaxUrl = base_url+'/Veraz/verVeraz/'+id_veraz;
    request.open("GET",ajaxUrl,true);
    request.send();
    request.onreadystatechange = function(){
        if(request.readyState == 4 && request.status == 200){
            let objData = JSON.parse(request.responseText);

            if(objData.status)
            {
               let estadoVeraz = objData.data.score > 200 ? 
                '<span class="badge badge-success">'+objData.data.score+'</span>' : 
                '<span class="badge badge-danger">'+objData.data.score+'</span>';

                document.querySelector("#verDocumento").innerHTML = objData.data.documento;
                document.querySelector("#verNombre").innerHTML = objData.data.nombres;
                document.querySelector("#verScore").innerHTML = estadoVeraz;
                document.querySelector("#verFecha").innerHTML = objData.data.fecha;
                document.querySelector("#verSexo").innerHTML = objData.data.sexo;
        
                $('#modalViewVeraz').modal('show');
            }else{
                swal("Error", objData.msg , "error");
            }
        }
    }
}

window.addEventListener('load', function() {
    
}, false);


function openModal()
{
    $('#modalFormVeraz').modal('show');    
}

function openModalPerfil(){
    $('#modalFormPerfil').modal('show');
}